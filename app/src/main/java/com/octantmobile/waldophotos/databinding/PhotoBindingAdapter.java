package com.octantmobile.waldophotos.databinding;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.octantmobile.waldophotos.R;
import com.octantmobile.waldophotos.model.Photo;
import com.squareup.picasso.Picasso;

public class PhotoBindingAdapter {

	@BindingAdapter({"photo"})
	public static void loadPhoto(ImageView view, Photo photo) {
		Picasso.with(view.getContext())
				.load(photo.url)
				.resize(photo.width, photo.height)
				// shortcut: in a real app, we would handle errors more thoughtfully
				.error(R.mipmap.ic_launcher)
				.into(view);
	}
}
