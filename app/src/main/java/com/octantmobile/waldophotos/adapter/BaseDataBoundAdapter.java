package com.octantmobile.waldophotos.adapter;


import android.databinding.OnRebindCallback;
import android.databinding.ViewDataBinding;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

/**
 * From: https://github.com/google/android-ui-toolkit-demos/tree/master/DataBinding/DataBoundRecyclerView
 *
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
abstract public class BaseDataBoundAdapter<T extends ViewDataBinding>
		extends RecyclerView.Adapter<DataBoundViewHolder<T>> {

	private static final Object DB_PAYLOAD = new Object();

	@Nullable
	private RecyclerView recyclerView;

	/**
	 * This is used to block items from updating themselves. RecyclerView wants to know when an item
	 * is invalidated and it prefers to refresh it via onRebind. It also helps with performance
	 * since data binding will not update views that are not changed.
	 */
	private final OnRebindCallback onRebindCallback = new OnRebindCallback() {
		@Override
		public boolean onPreBind(ViewDataBinding binding) {
			if (recyclerView == null || recyclerView.isComputingLayout()) {
				return true;
			}
			int childAdapterPosition = recyclerView.getChildAdapterPosition(binding.getRoot());
			if (childAdapterPosition == RecyclerView.NO_POSITION) {
				return true;
			}
			notifyItemChanged(childAdapterPosition, DB_PAYLOAD);
			return false;
		}
	};

	@Override
	@CallSuper
	public DataBoundViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
		DataBoundViewHolder<T> vh = DataBoundViewHolder.create(parent, viewType);
		return vh;
	}

	@Override
	public final void onBindViewHolder(DataBoundViewHolder<T> holder, int position,
	                                   List<Object> payloads) {
		bindItem(holder, position, payloads);
		holder.binding.executePendingBindings();
	}

	/**
	 * Override this method to handle binding your items into views
	 *
	 * @param holder   The ViewHolder that has the binding instance
	 * @param position The position of the item in the adapter
	 * @param payloads The payloads that were passed into the onBind method
	 */
	protected abstract void bindItem(DataBoundViewHolder<T> holder, int position,
	                                 List<Object> payloads);

	private boolean hasNonDataBindingInvalidate(List<Object> payloads) {
		for (Object payload : payloads) {
			if (payload != DB_PAYLOAD) {
				return true;
			}
		}
		return false;
	}

	@Override
	public final void onBindViewHolder(DataBoundViewHolder<T> holder, int position) {
		throw new IllegalArgumentException("just overridden to make final.");
	}

	@Override
	public void onAttachedToRecyclerView(RecyclerView recyclerView) {
		this.recyclerView = recyclerView;
	}

	@Override
	public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
		this.recyclerView = null;
	}
}