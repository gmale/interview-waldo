package com.octantmobile.waldophotos.adapter;

import android.view.ViewGroup;

import com.octantmobile.waldophotos.R;
import com.octantmobile.waldophotos.databinding.ViewAlbumItemBinding;
import com.octantmobile.waldophotos.model.Photo;

import java.util.List;

/**
 * @since 11/18/16.
 */
public class AlbumAdapter extends
		BaseDataBoundAdapter<ViewAlbumItemBinding> {

	private final OnPhotoClickListener listener;
	private List<Photo> photos;

	public interface OnPhotoClickListener {
		void onPhotoClick(Photo photo);
	}

	public AlbumAdapter(OnPhotoClickListener listener) {
		setHasStableIds(true);
		this.listener = listener;
	}


	//
	// Accessors
	//

	public void setPhotos(List<Photo> photos) {
		this.photos = photos;
		notifyDataSetChanged();
	}

	public List<Photo> getPhotos() {
		return photos;
	}


	//
	// BaseDataBoundAdapter Overrides
	//

	@Override
	protected void bindItem(DataBoundViewHolder<ViewAlbumItemBinding> holder, int position, List<Object> payloads) {
		Photo photo = photos.get(position);
		holder.binding.setPhoto(photo);
		holder.binding.photo.setOnClickListener(view -> listener.onPhotoClick(photo));
		// apply width/height to the holder's imageView
		ViewGroup.LayoutParams layoutParams = holder.binding.photo.getLayoutParams();
		layoutParams.width = photo.width;
		layoutParams.height = photo.height;
		holder.binding.photo.setLayoutParams(layoutParams);
	}

	@Override
	public int getItemCount() {
		return photos == null ? 0 : photos.size();
	}

	@Override
	public int getItemViewType(int position) {
		Photo photo = photos.get(position);
		// put w/h into an int and only reuse viewHolders that are the same size, which is more efficient
		return photo.width << 16 | photo.height;
	}

	@Override
	public DataBoundViewHolder<ViewAlbumItemBinding> onCreateViewHolder(ViewGroup parent, int viewType) {
		DataBoundViewHolder<ViewAlbumItemBinding> vh = super.onCreateViewHolder(parent, R.layout.view_album_item);
		return vh;
	}

	@Override
	public long getItemId(int position) {
		Photo photo = photos.get(position);
		long id = System.identityHashCode(photo) << 32 | (photo.width << 16) | (photo.height);
		return id;
	}

}
