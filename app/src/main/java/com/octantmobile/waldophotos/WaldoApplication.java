package com.octantmobile.waldophotos;

import android.app.Application;

/**
 * Probably won't need this class for this quick assignment. A production app would certainly need
 * a few things in here, like configuration of the IoC container.
 *
 * @since 11/19/16.
 */
public class WaldoApplication extends Application {
	private static WaldoApplication instance;

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
	}

	public WaldoApplication get() {
		return instance;
	}
}
