package com.octantmobile.waldophotos.activity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;

import com.octantmobile.waldophotos.R;
import com.octantmobile.waldophotos.adapter.AlbumAdapter;
import com.octantmobile.waldophotos.model.Album;
import com.octantmobile.waldophotos.model.Photo;
import com.octantmobile.waldophotos.service.AlbumService;
import com.octantmobile.waldophotos.service.ClientModule;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.ReplaySubject;
import rx.subjects.Subject;
import rx.subscriptions.CompositeSubscription;

import static android.support.v7.widget.StaggeredGridLayoutManager.VERTICAL;

public class MainActivity extends AppCompatActivity implements AlbumAdapter.OnPhotoClickListener {

	private final CompositeSubscription subscriptions = new CompositeSubscription();
	private RecyclerView photoList;
	private Subject<Album, Album> albumSubject;
	private ImageView headerImage;


	//
	// LifeCycle overrides
	//

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initHeader();
		initPhotoList();
		albumSubject = ReplaySubject.create();
		AlbumService.createApi(this, ClientModule.createOkHttpClient())
				// shortcut: normally, we'd pass this in or pull it from preferences
				.getAlbum(new Album("YWxidW06YTk4ZjE0ZjUtMzUwOC00NGVmLWIyMTctZmQ5OGQ5Y2VkOGU2"))
				// shortcut: normally, we'd filter the data further with RxJava or more specific network requests. Instead, here we quickly/manually transform the data
				.map(response -> response.toAlbum(getMaxColumnWidth()))
				.subscribeOn(Schedulers.io())
				.subscribe(albumSubject);

		// shortcut: normally, we'd display some type of "empty" content here, until the images load
	}

	@Override
	protected void onPause() {
		super.onPause();
		subscriptions.unsubscribe();
	}

	@Override
	public void onResume() {
		super.onResume();
		albumSubject
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(album -> {
					onAlbumLoaded(album);
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();

		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	//
	// View Logic
	//

	/**
	 * Determine the maximum width of each of our staggered grid columns, this will dictate the max
	 * width of each photo.
	 *
	 * @return the maximum column width in pixels
	 */
	public int getMaxColumnWidth() {
		DisplayMetrics outMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(outMetrics);
		int deviceWidth = outMetrics.widthPixels;
		// shortcut: hardcoded values here for simplicity. In a real app, we'd put these into resource files and ensure the columns are the proper size for tablets and landscape
		float paddingWidth = 32.0f * outMetrics.density;
		int maxWidth = Math.round((deviceWidth - paddingWidth) / 3.0f);
		return maxWidth;
	}

	/**
	 * Configure the non-album portion of the screen: the toolbar, header image and FAB
	 */
	private void initHeader() {
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		headerImage = (ImageView) findViewById(R.id.header_image);
		AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
		appBarLayout.addOnOffsetChangedListener((layout, verticalOffset) -> {
			if (Math.abs(verticalOffset) >= layout.getTotalScrollRange()) {
				updateHeader(((AlbumAdapter) photoList.getAdapter()).getPhotos());
			}
		});
		setSupportActionBar(toolbar);

		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		fab.setOnClickListener(view -> {
			Snackbar.make(view, "Share these with Grandma!", Snackbar.LENGTH_SHORT)
					.setAction("Action", null).show();
		});
	}

	/**
	 * Initialize the list of photos and the related layout manager and adapter.
	 */
	private void initPhotoList() {
		photoList = (RecyclerView) findViewById(R.id.album_photos);
		photoList.setLayoutManager(new StaggeredGridLayoutManager(3, VERTICAL));
		photoList.setAdapter(new AlbumAdapter(this));
	}

	/**
	 * Called when the album for this activity is loaded, containing the photos that we want to
	 * display.
	 *
	 * @param album the album that loaded, containing pictures to display.
	 */
	private void onAlbumLoaded(Album album) {
		if (album != null && album.photos != null) {
			AlbumAdapter adapter = (AlbumAdapter) photoList.getAdapter();
			adapter.setPhotos(album.photos);
			updateHeader(album.photos);
		}
	}

	/**
	 * just a quick touch to match the look/feel of the website: load a random image into the header
	 * with a blue tint
	 */
	private void updateHeader(List<Photo> photos) {
		if (photos != null && photos.size() > 0) {
			Photo randomPhoto = photos.get(new Random().nextInt(photos.size()));
			Picasso.with(headerImage.getContext())
					.load(randomPhoto.url)
					.error(R.mipmap.ic_launcher)
					.into(headerImage);
		}
	}

	// shortcut: just a bunch of sloppy quick code to show the photo detail dialog. In a real app, we'd use a dialog fragment to better handle the lifecycle and also incorporate some interesting animations so that the clicked items animate into and out of view
	@Override
	public void onPhotoClick(Photo photo) {
		final Dialog dialog = new Dialog(this);
		dialog.setCanceledOnTouchOutside(true);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.view_photo_detail_view);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		ImageView imageView = (ImageView) dialog.findViewById(R.id.photo);
		Picasso.with(imageView.getContext())
				.load(photo.url.replaceFirst("/medium2x/", "/large/")) //shortcut: hack to get the larger image. In a real app we'd provide an API in the model class to return the detail photo something like photo.getDetailUrl
				.error(R.mipmap.ic_launcher)
				.into(imageView);
		imageView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));
		dialog.show();
	}
}
