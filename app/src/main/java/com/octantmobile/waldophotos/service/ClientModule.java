package com.octantmobile.waldophotos.service;

import com.octantmobile.waldophotos.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Creates a client capable of logging requests for debug purposes
 */
public class ClientModule {

	// shortcut: these are static here but in reality this whole class would be converted into a Dagger Module
	public static OkHttpClient createOkHttpClient() {
		// we need this tidbit so that we can log requests, for sanity
		HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
		loggingInterceptor.setLevel(BuildConfig.DEBUG
				? HttpLoggingInterceptor.Level.BODY
				: HttpLoggingInterceptor.Level.NONE
		);

		OkHttpClient.Builder okHttpClientBuilder = new okhttp3.OkHttpClient.Builder()
				.addInterceptor(loggingInterceptor);
		return okHttpClientBuilder.build();
	}
}
