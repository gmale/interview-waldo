package com.octantmobile.waldophotos.service.graphql;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.RequestBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * Simple class to help retrofit make sense of GraphQL
 * @since 11/19/16.
 */
public class GraphQlConverterFactory extends Converter.Factory {

	@Override
	public Converter<?, RequestBody> requestBodyConverter(Type type,
	                                                      Annotation[] parameterAnnotations,
	                                                      Annotation[] methodAnnotations,
	                                                      Retrofit retrofit) {
		return isQuery(type) ? GraphQlRequestBodyConverter.INSTANCE : null;
	}

	private static boolean isQuery(Type type) {

		return type instanceof Class<?>
				&& Queryable.class.isAssignableFrom((Class<?>)type);
	}
}