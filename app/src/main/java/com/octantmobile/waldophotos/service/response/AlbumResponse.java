package com.octantmobile.waldophotos.service.response;

import com.octantmobile.waldophotos.model.Album;
import com.octantmobile.waldophotos.model.Photo;

import java.util.ArrayList;
import java.util.List;

/**
 * @since 11/19/16.
 */
public class AlbumResponse {
	// shortcut: just a quick model hierarchy to pull out data exactly as it is in the primary request
	Data data;
	public class Data {
		AlbumData album;
	}
	public class AlbumData {
		String id;
		String name;
		PhotosResponse photos;
	}
	public class PhotosResponse {
		List<Record> records;
	}
	public class Record {
		List<PhotoRecord> urls;
	}
	public class PhotoRecord {
		String size_code;
		String url;
		int width;
		int height;
		float quality;
	}

	// shortcut: in real life, we'd get the data the way we want it, elsewhere. If we do end up using a translation layer, it would be way more robust than this!
	/** this method is doing some lazy quick dirty parsing. A real app would never behave this way! */
	public Album toAlbum(int maxWidth) {
		// shortcut: if this were a real app we'd transform the data in a more robust way
		Album album = null;
		if(data != null && data.album != null && data.album.id != null) {
			String sizeCode = "medium2x";
			album = new Album(data.album.id, data.album.name);
			album.photos = new ArrayList<>();
			if(data.album.photos != null && data.album.photos.records != null) {
				for (Record record : data.album.photos.records) {
					// find the URL that matches the size we want
					if (record.urls != null) {
						for (PhotoRecord photoRecord : record.urls) {
							if (sizeCode.equalsIgnoreCase(photoRecord.size_code) && photoRecord.url != null) {
								int w = photoRecord.width;
								int h = photoRecord.height;
								if (w > maxWidth) {
									h = Math.round( ((float)maxWidth) / w * h );
									w = maxWidth;
								}
								album.photos.add(new Photo(photoRecord.url, w, h));
							}
						}
					}
				}
			}
		}
		return album;
	}
}
