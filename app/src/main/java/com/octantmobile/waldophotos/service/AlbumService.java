package com.octantmobile.waldophotos.service;

import android.content.Context;

import com.octantmobile.waldophotos.R;
import com.octantmobile.waldophotos.service.graphql.GraphQlConverterFactory;
import com.octantmobile.waldophotos.service.graphql.Queryable;
import com.octantmobile.waldophotos.service.response.AlbumResponse;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

/**
 * @since 11/19/16.
 */
public class AlbumService {

	public static AlbumApi createApi(Context context, OkHttpClient client) {
		// shortcut: retrofit would probably be injected by Dagger (or an IoC framework)
		Retrofit retrofit = new Retrofit.Builder()
				// pulling from string resources is an easy way to use different servers for each
				// app flavor when needed
				.baseUrl(context.getString(R.string.album_server_url))
				.client(client)
				.addCallAdapterFactory(RxJavaCallAdapterFactory.create())
				.addConverterFactory(new GraphQlConverterFactory())
				.addConverterFactory(GsonConverterFactory.create())
				.build();
		return retrofit.create(AlbumApi.class);
	}

	public interface AlbumApi {
		@POST("/gql")
		@Headers({"Content-Type: application/graphql", "Cookie: __dev.waldo.auth__=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50X2lkIjoiYmZmNjNkNDYtMjYzZS00ZDBkLThmYjQtMjJhMjA3ZTBjZDFmIiwicm9sZXMiOlsiYWRtaW5pc3RyYXRvciJdLCJpc3MiOiJ3YWxkbzpjb3JlIiwiZ3JhbnRzIjpbImFsYnVtczpkZWxldGU6KiIsImFsYnVtczpjcmVhdGU6KiIsImFsYnVtczplZGl0OioiLCJhbGJ1bXM6dmlldzoqIl0sImV4cCI6MTQ4MjA1MTYyOSwiaWF0IjoxNDc5NDU5NjI5fQ.tttJca7gF-WWPqrAtS-8vlJirjQWcU1xhDxE44lIRPc; path=/; domain=.dev.waldo.photos; Expires=Sun Dec 18 2016 02:00:29 GMT-0700 (MST);"})
		Observable<AlbumResponse> getAlbum(@Body Queryable query);
	}
}
