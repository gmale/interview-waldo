package com.octantmobile.waldophotos.service.graphql;

/**
 * @since 11/19/16.
 */
public interface Queryable {
	String toQuery();
}
