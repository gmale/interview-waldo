package com.octantmobile.waldophotos.service.graphql;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Converter;

/**
 * Simple class to help retrofit make sense of GraphQL
 * @since 11/19/16.
 */
public class GraphQlRequestBodyConverter implements Converter<Queryable, RequestBody> {
	private static final MediaType PLAIN_TEXT_MEDIA_TYPE = MediaType.parse("text/plain; charset=UTF-8");
	static final GraphQlRequestBodyConverter INSTANCE = new GraphQlRequestBodyConverter();
	@Override
	public RequestBody convert(Queryable value) throws IOException {
		return RequestBody.create(PLAIN_TEXT_MEDIA_TYPE, value.toQuery());
	}
}
