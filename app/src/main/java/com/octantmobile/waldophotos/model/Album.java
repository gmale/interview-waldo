package com.octantmobile.waldophotos.model;

import com.octantmobile.waldophotos.service.graphql.Queryable;

import java.util.List;

/**
 * @since 11/19/16.
 */
//shortcut: in a real app this would likely have more model logic, particularly for accessing large and thumbnail photos
public class Album implements Queryable {
	public String id;
	public String name;
	public List<Photo> photos;

	public Album(String id) {
		this(id, null);
	}

	public Album(String id, String name) {
		this.id = id;
		this.name = name;
	}

	@Override
	public String toQuery() {
		// shortcut: in a real app we'd create this in a more robust way, probably with something like a QueryBuilder class
		return "query {\n" +
				"  album(id: \""+id+"\") {\n" +
				"    id,\n" +
//				"    name,\n" +
				"    photos {\n" +
				"      records {\n" +
				"       urls {\n" +
				"        size_code\n" +
				"        url\n" +
				"        width\n" +
				"        height\n" +
//				"        quality\n" +
//				"        mime\n" +
				"      } \n" +
				"      }\n" +
				"    }\n" +
				"  }\n" +
				"}";
	}
}
