package com.octantmobile.waldophotos.model;

/**
 * @since 11/19/16.
 */
// shortcut: this would likely want to implement Queryable in a real app
public class Photo {
	public String url;
	public int width;
	public int height;

	public Photo(String url, int width, int height) {
		this.url = url;
		this.width = width;
		this.height = height;
	}
}
