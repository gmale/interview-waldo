# Sample Android project for Waldo Photos

----

This is a sample infinite scrolling image loading app for Waldo Photos. It integrates with the GraphQL API and renders an album's contents into an infinitely scrolling list. The UI has also incorporated the visual look and feel of the waldophotos.com website.

# Demo

An on-device demo video of the app can be found here:    
https://www.youtube.com/watch?v=rxS7cwxCT9I

apk can be downloaded here:    
https://bitbucket.org/gmale/interview-waldo/downloads/waldo.apk

# License

I release this code to be used by anyone at Waldo Photos for any purpose and have attached a permissive license to explicitly indicate that intent.
